import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { PropertieCardComponent } from './components/property-card/property-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { HomeComponent } from './components/home/home.component';
import { PropertyCardLongComponent } from './components/property-card-long/property-card-long.component';
import { PropertyCardDetailedComponent } from './components/property-card-detailed/property-card-detailed.component';
import { PostPropertyComponent } from './components/post-property/post-property.component';
import { AllServicesComponent } from './components/all-services/all-services.component';
import { CategoryViewComponent } from './components/category-view/category-view.component';
import { ServiceCardLongComponent } from './components/service-card-long/service-card-long.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PropertyDetailsComponent } from './components/property-details/property-details.component';
import { ServiceDetailsComponent } from './components/service-details/service-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PropertieCardComponent,
    SearchPropertyComponent,
    HomeComponent,
    PropertyCardLongComponent,
    PropertyCardDetailedComponent,
    PostPropertyComponent,
    AllServicesComponent,
    CategoryViewComponent,
    ServiceCardLongComponent,
    LoginComponent,
    RegisterComponent,
    PropertyDetailsComponent,
    ServiceDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
