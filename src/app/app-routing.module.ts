import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { HomeComponent } from './components/home/home.component';
import { PostPropertyComponent } from './components/post-property/post-property.component';
import { AllServicesComponent } from './components/all-services/all-services.component';
import { CategoryViewComponent } from './components/category-view/category-view.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PropertyDetailsComponent } from './components/property-details/property-details.component';
import { ServiceDetailsComponent } from './components/service-details/service-details.component';

const routes: Routes = [
  { path: 'SearchProperty', component: SearchPropertyComponent },
  { path: 'Home', component: HomeComponent },
  { path: 'PostProperty', component: PostPropertyComponent },
  { path: 'AllServices', component: AllServicesComponent },
  { path: 'CategoryView', component: CategoryViewComponent },
  { path: 'Login', component: LoginComponent },
  { path: 'Register', component: RegisterComponent },
  { path: 'PropertyDetails', component: PropertyDetailsComponent },
  { path: 'ServiceDetails', component: ServiceDetailsComponent },
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
