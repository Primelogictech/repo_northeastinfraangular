import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyCardLongComponent } from './property-card-long.component';

describe('PropertyCardLongComponent', () => {
  let component: PropertyCardLongComponent;
  let fixture: ComponentFixture<PropertyCardLongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyCardLongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyCardLongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
