import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-property',
  templateUrl: './search-property.component.html',
  styleUrls: ['./search-property.component.scss']
})
export class SearchPropertyComponent implements OnInit {
  public ViewType=false;
  constructor() { }

  ngOnInit(): void {

  }

  changeView(){
    this.ViewType = !this.ViewType
  }

}
