import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyCardDetailedComponent } from './property-card-detailed.component';

describe('PropertyCardDetailedComponent', () => {
  let component: PropertyCardDetailedComponent;
  let fixture: ComponentFixture<PropertyCardDetailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyCardDetailedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyCardDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
