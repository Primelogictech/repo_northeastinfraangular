import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCardLongComponent } from './service-card-long.component';

describe('ServiceCardLongComponent', () => {
  let component: ServiceCardLongComponent;
  let fixture: ComponentFixture<ServiceCardLongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceCardLongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCardLongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
